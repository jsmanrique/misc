# meetups_in_your_city.py
# Copyright (C) 2020 J. Manrique López de la Fuente
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

#!/usr/bin/env python
# coding: utf-8

"""meetups_in_your_city.py: Get a .csv files with all the Meetup groups
in a given city for a given topic
"""

__author__ = "J. Manrique López de la Fuente"
__copyright__ = "Copyright (C) 2020, J. Manrique López de la Fuente"
__license__ = "GPL"

__version__ = "0.0.1"
__maintainer__ = "J. Manrique López de la Fuente"

import csv

import urllib
from bs4 import BeautifulSoup

import argparse
import configparser

def parse_args(args):
    parser = argparse.ArgumentParser(description = 'Get a csv file with all the Meetup groups for a given topic in a given city')
    parser.add_argument('-t', '--topic', dest = 'topic', help = 'Topic', required = True)
    parser.add_argument('-C', '--country', dest = 'country', help = 'Country in lower letters Alpha-2 code ISO 3166', required = True)
    parser.add_argument('-c', '--city', dest = 'city', help = 'City or State/City', required = True)
    parser.add_argument('-o', '--output', dest = 'filename', default = 'default', help = 'Output .csv file name')

    return parser.parse_args()

def get_page_soup(topic, country, city):
    url = 'https://www.meetup.com/es-ES/topics/{}/{}/{}'.format(topic, country, city)
    try:
        html_code = urllib.request.urlopen(url)
    except urllib.error.HTTPError as err:
        print(err)
        exit()

    doc = html_code.read()
    
    soup = BeautifulSoup(doc,'html.parser')

    return soup

def has_class(tag, value='groupCard'):
    if tag.has_attr('class') and value in tag['class']:
        return tag

def meetups(soup):
    meetups = []
    for item in soup.find_all(has_class):
        meetups.append([item['data-name'], item['data-urlname']])
    #print(len(meetups))
    return meetups

def remove_slashs(string):
    return string.replace('/','-')

def main(args):
    args = parse_args(args)

    soup = get_page_soup(args.topic, args.country, args.city)

    if args.filename == 'default':
        filename = 'meetups-about-{}-in-{}-{}.csv'.format(args.topic, args.country, remove_slashs(args.city))
    else:
        filename = '{}.csv'.format(args.filename)

    with open(filename, mode='w') as csv_file:
        meetup_writer = csv.writer(csv_file)
        for meetup in meetups(soup):
            meetup_writer.writerow([meetup[0], meetup[1], 'https://www.meetup.com/{}'.format(meetup[1])])

if __name__ == '__main__':
    import sys
    main(sys.argv[1:])
    